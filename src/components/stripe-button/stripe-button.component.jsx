import React from "react";
import StripeCheckout from "react-stripe-checkout";


const StripeCheckoutButton = ({ price }) => {
    const priceForStripe = price * 100;
    const publishableKey = 'pk_test_51OZ2HOEmkZARgJePqHzO65sZFPtvwfSOogLjl7NF7xfFY2xntErC7OTNVleuQKHrVjTJNbNgrY2ePY29G8rNcecL009E3yBOIq'

    const onToken = token => {
        console.log(token)
        alert('Paymen Successful')
    }
    return (
        <StripeCheckout 
            label="Pay Now"
            name="CRWN Clothing Ltd"
            billingAddress
            shippingAddress
            image="https://svgshare.com/i/CUz.svg"
            description={`Your total is $${price}`}
            amount={priceForStripe}
            panelLabel="Pay Now"
            token={onToken}
            stripeKey={publishableKey}
        />
    )
}

export default StripeCheckoutButton