import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

//old
// const config = {
//     apiKey: "AIzaSyBXLF8OxEal_Usvmj5kfnmDfoKTSz3cogU",
//     authDomain: "crwn-db-fc61a.firebaseapp.com",
//     projectId: "crwn-db-fc61a",
//     storageBucket: "crwn-db-fc61a.appspot.com",
//     messagingSenderId: "388895307666",
//     appId: "1:388895307666:web:731f2c429eb1dd29b5334b",
//     measurementId: "G-N9HEL1HYEF"
// };
//new
const config = {
    apiKey: "AIzaSyCyLRnYZp9YQhN9H7LjiaLGoR778SPCuqA",
    authDomain: "crwn-clothing-db-6b924.firebaseapp.com",
    projectId: "crwn-clothing-db-6b924",
    storageBucket: "crwn-clothing-db-6b924.appspot.com",
    messagingSenderId: "690738060382",
    appId: "1:690738060382:web:89bb3c72aa2ad4b8fc68cf",
    measurementId: "G-BT2J5WSV9L"
};

firebase.initializeApp(config)

export const createUserProfileDocument = async (userAuth, additionalData) => {
    if (!userAuth) return;

    const userRef = firestore.doc(`users/${userAuth.uid}`)
    const snapShot = await userRef.get();

    console.log(snapShot)
    if (!snapShot.exists) {
        const { displayName, email } = userAuth
        const createAt = new Date()

        try {
            await userRef.set({
                displayName,
                email,
                createAt,
                ...additionalData
            })
        } catch (error) {
            console.log('error creating user', error.message)
        }
    }
    return userRef;
}

// firebase.initializeApp(config);

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: 'select_account' });
export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;